
#pragma once

#include <AzCore/std/smart_ptr/make_shared.h>
#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/ComponentBus.h>
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Math/Vector2.h>

#include "DynamicMesh/DynamicMeshBus.h"

namespace ProceduralMesh
{
    enum class PrimitiveType : uint32_t
    {
        Empty = 0,
        Plane,
        Disk,
        Torus
    };

    class Primitive
    {
    public:
        AZ_CLASS_ALLOCATOR(Primitive, AZ::SystemAllocator, 0);
        AZ_RTTI(Primitive, "{F2164898-74C5-454B-94F4-7E481F76D178}");
        virtual ~Primitive() = default;

        virtual PrimitiveType GetType() const { return PrimitiveType::Empty; }
        virtual void CreateMesh(const AZ::EntityId entityId, const DynamicMesh::DynamicMeshId id) const {}

        // used to notify of changed events (editor only)
        virtual AZ::Crc32 OnChanged() { return m_onChangedCallback ? m_onChangedCallback() : AZ::Crc32(); }
        AZStd::function<AZ::Crc32(void)> m_onChangedCallback = nullptr;
        virtual AZStd::shared_ptr<Primitive> GetShared() const { return AZStd::make_shared<Primitive>(*this); };
    };

    class Plane : public Primitive
    {
    public:
        AZ_CLASS_ALLOCATOR(Plane, AZ::SystemAllocator, 0);
        AZ_RTTI(Plane, "{9541F05A-2F1D-4487-AA13-0715F295288E}", Primitive);
        ~Plane() override = default;

        PrimitiveType GetType() const override { return PrimitiveType::Plane; }
        void CreateMesh(const AZ::EntityId entityId,const DynamicMesh::DynamicMeshId id) const override;
        AZ::Crc32 OnChanged() override { return Primitive::OnChanged();  }
        AZStd::shared_ptr<Primitive> GetShared() const override { return AZStd::make_shared<Plane>(*this); };

        float m_width = 1.f;
        float m_height = 1.f;
        uint32_t m_segments = 1;
    };

    class Disk : public Primitive
    {
    public:
        AZ_CLASS_ALLOCATOR(Disk, AZ::SystemAllocator, 0);
        AZ_RTTI(Disk, "{C0DB6D07-8378-4575-84F0-382D97B8F0C3}", Primitive);
        ~Disk() override = default;

        PrimitiveType GetType() const override { return PrimitiveType::Disk; }
        void CreateMesh(const AZ::EntityId entityId, const DynamicMesh::DynamicMeshId id) const override;
        AZ::Crc32 OnChanged() override { return Primitive::OnChanged();  }
        AZStd::shared_ptr<Primitive> GetShared() const override { return AZStd::make_shared<Disk>(*this); };

        float m_innerRadius = 0.f;
        float m_outerRadius = 1.f;
        uint32_t m_segments = 16;
    };

    class Torus : public Primitive
    {
    public:
        AZ_CLASS_ALLOCATOR(Disk, AZ::SystemAllocator, 0);
        AZ_RTTI(Torus, "{C440AD45-3FA7-4695-B556-E198033D3210}", Primitive);
        ~Torus() override = default;

        PrimitiveType GetType() const override { return PrimitiveType::Torus; }
        void CreateMesh(const AZ::EntityId entityId, const DynamicMesh::DynamicMeshId id) const override;
        AZ::Crc32 OnChanged() override { return Primitive::OnChanged();  }
        AZStd::shared_ptr<Primitive> GetShared() const override { return AZStd::make_shared<Torus>(*this); };

        // radius in x and y axis
        AZ::Vector2 m_radius = AZ::Vector2(1.f, 1.f);

        // thickness in pixels
        float m_thickness = 0.1f;

        // number of segments 
        // TOOD: some kind of lod for greater tesselation near camera might be nice 
        uint32_t m_segments = 32;

        // number of sides 
        uint32_t m_sides = 6;
    };


    class ProceduralPrimitiveRequests
        : public AZ::ComponentBus
    {
    public:
        /**
        * Set the primitive to generate a dynamic mesh for
        * @param the Primitive to use to generate a dynamic mesh.
        */
        virtual void SetPrimitive(const Primitive& primitive) = 0;
    };
    using ProceduralPrimitiveRequestBus = AZ::EBus<ProceduralPrimitiveRequests>;
} // namespace flite
