
#include "ProceduralMesh_precompiled.h"

#include <IRenderer.h> // needed for vertex formats, including "VertexFormats.h" produces compile errors 

#include "EditorProceduralMeshComponent.h"

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/RTTI/BehaviorContext.h>
#include <AzCore/Math/MathUtils.h>

namespace ProceduralMesh 
{
    /*
    AZ::Crc32 Primitive::OnChanged() 
    { 
        EditorProceduralMeshNotificationBus::Event(m_entityId, &EditorProceduralMeshNotificationBus::Events::OnPrimitiveChanged);

        return AZ::Edit::PropertyRefreshLevels::EntireTree;
    }
    */

    /*
    AZ::Crc32 EditorProceduralMeshComponentConfig::GetVisibility(PrimitiveType type) const
    {
        return m_primitiveType == type ? AZ::Edit::PropertyVisibility::ShowChildrenOnly : AZ::Edit::PropertyVisibility::Hide;
    }
    
    AZ::Crc32 EditorProceduralMeshComponentConfig::GetDiskVisibility() const 
    { 
        return GetVisibility(PrimitiveType::Disk); 
    }

    AZ::Crc32 EditorProceduralMeshComponentConfig::GetPlaneVisibility() const 
    { 
        return GetVisibility(PrimitiveType::Plane); 
    }

    void EditorProceduralMeshComponentConfig::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            //serialize->Class<EditorProceduralMeshComponentConfig, ProceduralMeshComponentConfig>()
            //    ->Version(1)
            //serialize->Class<EditorProceduralMeshComponentConfig, AZ::ComponentConfig>()
                //    ->Version(1)
                //    ->Field("Primitive Type", &ProceduralMeshComponentConfig::m_primitiveType)
                //    ->Field("Plane", &ProceduralMeshComponentConfig::m_plane)
                //    ->Field("Disk", &ProceduralMeshComponentConfig::m_disk)
                //    ;
                ;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<EditorProceduralMeshComponentConfig>("ProceduralMeshConfig", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))

                    // Plane
                    ->DataElement(0, &EditorProceduralMeshComponentConfig::m_plane)
                    ->Attribute(AZ::Edit::Attributes::Visibility, &EditorProceduralMeshComponentConfig::GetPlaneVisibility)

                    // Disk
                    ->DataElement(0, &EditorProceduralMeshComponentConfig::m_disk)
                    ->Attribute(AZ::Edit::Attributes::Visibility, &EditorProceduralMeshComponentConfig::GetDiskVisibility)
                    ;

                ec->Class<Plane>("Plane", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ->DataElement(0, &Plane::m_width, "Width", "Width")
                    ->DataElement(0, &Plane::m_height, "Height", "Height")
                    ->DataElement(0, &Plane::m_segments, "Segments", "Segments")
                    ;

                ec->Class<Disk>("Disk", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ->DataElement(0, &Disk::m_innerRadius, "Inner radius", "Inner radius")
                    ->DataElement(0, &Disk::m_outerRadius, "Outer radius", "Outer radius")
                    ->DataElement(0, &Disk::m_segments, "Segments", "Segments")
                    ;
            }
        }
    }
    */

    void EditorProceduralMeshComponent::Reflect(AZ::ReflectContext* context)
    {
        //EditorProceduralMeshComponentConfig::Reflect(context);

        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<EditorProceduralMeshComponent, AZ::Component>()
                ->Version(1)
                //->Field("Config", &EditorProceduralMeshComponent::m_config)
                ->Field("Primitive Type", &EditorProceduralMeshComponent::m_primitiveType)
                ->Field("Plane", &EditorProceduralMeshComponent::m_plane)
                ->Field("Disk", &EditorProceduralMeshComponent::m_disk)
                ->Field("Torus", &EditorProceduralMeshComponent::m_torus)
                ;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<EditorProceduralMeshComponent>("ProceduralMesh", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::Category, "Rendering")
                        ->Attribute(AZ::Edit::Attributes::PreferNoViewportIcon, true)
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->DataElement(AZ::Edit::UIHandlers::ComboBox, &EditorProceduralMeshComponent::m_primitiveType, "Primitive", "Primitive")
                        ->Attribute(AZ::Edit::Attributes::ChangeNotify, &EditorProceduralMeshComponent::OnPrimitiveTypeChanged)
                        ->EnumAttribute(PrimitiveType::Empty, "None")
                        ->EnumAttribute(PrimitiveType::Plane, "Plane")
                        ->EnumAttribute(PrimitiveType::Disk, "Disk")
                        ->EnumAttribute(PrimitiveType::Torus, "Torus")
                    //->DataElement(0, &EditorProceduralMeshComponent::m_config, "Config", "Config")
                    //    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    //    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))

                    // Plane
                    ->DataElement(0, &EditorProceduralMeshComponent::m_plane)
                    ->Attribute(AZ::Edit::Attributes::Visibility, &EditorProceduralMeshComponent::GetPlaneVisibility)

                    // Disk
                    ->DataElement(0, &EditorProceduralMeshComponent::m_disk)
                    ->Attribute(AZ::Edit::Attributes::Visibility, &EditorProceduralMeshComponent::GetDiskVisibility)

                    //Torus 
                    ->DataElement(0, &EditorProceduralMeshComponent::m_torus)
                    ->Attribute(AZ::Edit::Attributes::Visibility, &EditorProceduralMeshComponent::GetTorusVisibility)
                    ;

                ec->Class<Plane>("Plane", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ->DataElement(0, &Plane::m_width, "Width", "Width")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Plane::OnChanged)
                    ->DataElement(0, &Plane::m_height, "Height", "Height")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Plane::OnChanged)
                    ->DataElement(0, &Plane::m_segments, "Segments", "Segments")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Plane::OnChanged)
                    ;

                ec->Class<Disk>("Disk", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ->DataElement(0, &Disk::m_innerRadius, "Inner radius", "Inner radius")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Disk::OnChanged)
                    ->DataElement(0, &Disk::m_outerRadius, "Outer radius", "Outer radius")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Disk::OnChanged)
                    ->DataElement(0, &Disk::m_segments, "Segments", "Segments")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Disk::OnChanged)
                    ;

                ec->Class<Torus>("Torus", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ->DataElement(0, &Torus::m_radius, "Radius", "Radius")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Torus::OnChanged)
                    ->DataElement(0, &Torus::m_thickness, "Thickness", "Thickness")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Torus::OnChanged)
                    ->DataElement(0, &Torus::m_segments, "Segments", "Segments")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Torus::OnChanged)
                    ->DataElement(0, &Torus::m_sides, "Sides", "Sides")
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &Torus::OnChanged)
                    ;
            }
        }
    }

    void EditorProceduralMeshComponent::Init()
    {
        m_disk.m_onChangedCallback = AZStd::bind(&EditorProceduralMeshComponent::OnPrimitiveChanged, this);
        m_plane.m_onChangedCallback = AZStd::bind(&EditorProceduralMeshComponent::OnPrimitiveChanged, this);
        m_torus.m_onChangedCallback = AZStd::bind(&EditorProceduralMeshComponent::OnPrimitiveChanged, this);
    }

    void EditorProceduralMeshComponent::Activate()
    {
        ProceduralPrimitiveRequestBus::Handler::BusConnect(GetEntityId());

        OnPrimitiveChanged();
    }

    void EditorProceduralMeshComponent::Deactivate()
    {
        if (!m_meshId.IsNull())
        {
            DynamicMesh::DynamicMeshRequestBus::Event(GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::DestroyDynamicMesh, m_meshId);
            m_meshId = DynamicMesh::DynamicMeshId::CreateNull();
        }

        ProceduralPrimitiveRequestBus::Handler::BusDisconnect();
    }

    AZ::Crc32 EditorProceduralMeshComponent::OnPrimitiveChanged()
    {
        // TODO refresh after a timeout of 500ms or more
        OnPrimitiveTypeChanged();

        return AZ::Edit::PropertyRefreshLevels::None;
    }

    AZ::Crc32 EditorProceduralMeshComponent::OnPrimitiveTypeChanged()
    { 
        m_primitiveType = m_primitiveType;

        if (m_primitiveType == PrimitiveType::Plane)
        {
            SetPrimitive(m_plane);
        }
        else if (m_primitiveType == PrimitiveType::Disk)
        {
            SetPrimitive(m_disk);
        }
        else if (m_primitiveType == PrimitiveType::Torus)
        {
            SetPrimitive(m_torus);
        }
        else
        {
            SetPrimitive(Primitive());
        }

        return AZ::Edit::PropertyRefreshLevels::EntireTree; 
    };

    void EditorProceduralMeshComponent::SetPrimitive(const Primitive& primitive)
    {
        m_primitiveType = primitive.GetType();

        // destroy the mesh we currently have
        if (!m_meshId.IsNull())
        {
            DynamicMesh::DynamicMeshRequestBus::Event(GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::DestroyDynamicMesh, m_meshId);
            m_meshId = DynamicMesh::DynamicMeshId::CreateNull();
        }

        if (m_primitiveType != PrimitiveType::Empty)
        {
            DynamicMesh::DynamicMeshRequestBus::EventResult(m_meshId, GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::CreateDynamicMesh);
            if (!m_meshId.IsNull())
            {
                primitive.CreateMesh(GetEntityId(), m_meshId);
            }
        }
    }

    //bool EditorProceduralMeshComponent::ReadInConfig(const AZ::ComponentConfig* baseConfig)
    //{
    //    if (auto config = azrtti_cast<const EditorProceduralMeshComponentConfig*>(baseConfig))
    //    {
    //        static_cast<EditorProceduralMeshComponentConfig>(m_config) = *config;
    //        return true;
    //    }
    //    return false;
    //}

    //bool EditorProceduralMeshComponent::WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const
    //{
    //    if (auto config = azrtti_cast<EditorProceduralMeshComponentConfig*>(outBaseConfig))
    //    {
    //        *config = m_config;
    //        return true;
    //    }
    //    return false;
    //}

    void EditorProceduralMeshComponent::BuildGameEntity(AZ::Entity* gameEntity)
    {
        //gameEntity->CreateComponent<ProceduralMeshComponent>()->SetConfiguration(m_config);
        if (m_primitiveType == PrimitiveType::Disk)
        {
            gameEntity->CreateComponent<ProceduralMeshComponent>(m_disk);
        }
        else if (m_primitiveType == PrimitiveType::Plane)
        {
            gameEntity->CreateComponent<ProceduralMeshComponent>(m_plane);
        }
        else if (m_primitiveType == PrimitiveType::Torus)
        {
            gameEntity->CreateComponent<ProceduralMeshComponent>(m_torus);
        }
        else
        {
            gameEntity->CreateComponent<ProceduralMeshComponent>();
        }
    }

    AZ::Crc32 EditorProceduralMeshComponent::GetVisibility(PrimitiveType type) const
    {
        return m_primitiveType == type ? AZ::Edit::PropertyVisibility::ShowChildrenOnly : AZ::Edit::PropertyVisibility::Hide;
    }
    
    AZ::Crc32 EditorProceduralMeshComponent::GetDiskVisibility() const
    { 
        return GetVisibility(PrimitiveType::Disk); 
    }

    AZ::Crc32 EditorProceduralMeshComponent::GetPlaneVisibility() const
    { 
        return GetVisibility(PrimitiveType::Plane); 
    }

    AZ::Crc32 EditorProceduralMeshComponent::GetTorusVisibility() const
    { 
        return GetVisibility(PrimitiveType::Torus); 
    }
}
