
#include "ProceduralMesh_precompiled.h"
#include <platform_impl.h>

#include <AzCore/Memory/SystemAllocator.h>

#include "ProceduralMeshComponent.h"

#if defined(PROCEDURALMESH_EDITOR)
#include "EditorProceduralMeshComponent.h"
#endif

#include <IGem.h>

namespace ProceduralMesh
{
    class ProceduralMeshModule
        : public CryHooksModule
    {
    public:
        AZ_RTTI(ProceduralMeshModule, "{0D3E6D11-F3E9-4560-9EC7-F91C0D08933C}", CryHooksModule);
        AZ_CLASS_ALLOCATOR(ProceduralMeshModule, AZ::SystemAllocator, 0);

        ProceduralMeshModule()
            : CryHooksModule()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                ProceduralMeshComponent::CreateDescriptor(),
#if defined(PROCEDURALMESH_EDITOR)
                EditorProceduralMeshComponent::CreateDescriptor()
#endif
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(ProceduralMesh_c303cf51dcba41db8f58549bcce6a6f0, ProceduralMesh::ProceduralMeshModule)
