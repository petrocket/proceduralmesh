
#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/ComponentBus.h>

namespace ProceduralMesh
{
    class EditorProceduralMeshNotifications
        : public AZ::ComponentBus
    {
    public:
        /**
        * Notify when primitive values have changed 
        */
        virtual void OnPrimitiveChanged() = 0;
    };

    using EditorProceduralMeshNotificationBus = AZ::EBus<EditorProceduralMeshNotifications>;
} // namespace flite
