#include "ProceduralMesh_precompiled.h"

#include <IRenderer.h> // needed for vertex formats, including "VertexFormats.h" produces compile errors 

#include "ProceduralMeshComponent.h"

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Math/MathUtils.h>
#include <AzCore/RTTI/BehaviorContext.h>
#include <AzCore/Component/TransformBus.h>
#include <I3DEngine.h>
#include <MathConversion.h>

#pragma optimize("", off)

namespace ProceduralMesh 
{
    ProceduralMeshComponent::ProceduralMeshComponent(const Primitive& primitive)
    {
        m_primitive = primitive.GetShared();
    }
    
    /*
    void ProceduralMeshComponentConfig::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<ProceduralMeshComponentConfig, AZ::ComponentConfig>()
                ->Version(1)
                ->Field("Primitive Type", &ProceduralMeshComponentConfig::m_primitiveType)
                ;
        }

        if (AZ::BehaviorContext* behaviorContext = azrtti_cast<AZ::BehaviorContext*>(context))
        {
            using Primitive = ProceduralMesh::Primitive;
            behaviorContext->Class<Primitive>("ProceduralPrimitive")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ;

            behaviorContext->Class<Plane>("ProceduralPlane")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ->Property("width", BehaviorValueProperty(&Plane::m_width))
                ->Property("height", BehaviorValueProperty(&Plane::m_height))
                ->Property("segments", BehaviorValueProperty(&Plane::m_segments))
                ;

            behaviorContext->Class<Disk>("ProceduralDisk")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ->Property("innerRadius", BehaviorValueProperty(&Disk::m_innerRadius))
                ->Property("outerRadius", BehaviorValueProperty(&Disk::m_outerRadius))
                ->Property("segments", BehaviorValueProperty(&Disk::m_segments))
                ;
        }
    }
    */

    static float ComputeConstantScale(const Vec3& v, const Matrix44A& matView, const Matrix44A& matProj, const uint32 wndXRes)
    {
        Vec4 vCam0;
        mathVec3TransformF(&vCam0, &v, &matView);

        Vec4 vCam1(vCam0);
        vCam1.x += 1.0f;

        const float a = vCam0.y * matProj.m10 + vCam0.z * matProj.m20 + matProj.m30;
        const float b = vCam0.y * matProj.m13 + vCam0.z * matProj.m23 + matProj.m33;

        float c0((vCam0.x * matProj.m00 + a) / (vCam0.x * matProj.m03 + b));
        float c1((vCam1.x * matProj.m00 + a) / (vCam1.x * matProj.m03 + b));

        float s = (float)wndXRes * (c1 - c0);

        const float epsilon = 0.001f;
        return (fabsf(s) >= epsilon) ? 1.0f / s : 1.0f / epsilon;
    }


    void Torus::CreateMesh(const AZ::EntityId entityId, DynamicMesh::DynamicMeshId meshId) const
    {

        // hijack this to build a torus instead
        const uint32_t numSegments = std::max<uint32_t>(m_segments, 3);
        const uint32_t numSides = std::max<uint32_t>(m_sides, 3); 
        const float tubeRadius = m_thickness;// 0.5f;

        AZStd::vector<SVF_P3F_C4B_T2F> vertices;
        vertices.clear();

        // we duplicate the initial verts at the start and end for simplicity
        // TODO just fix the indices instead
        const uint32_t numVertices = (numSegments + 1) * numSides;
        vertices.reserve(numVertices);

        float startAngle = 0.f;
        float segmentAngleDelta = AZ::Constants::TwoPi / (float)numSegments;
        float sidesAngleDelta = AZ::Constants::TwoPi / (float)numSides;

        SVF_P3F_C4B_T2F vertex;
        Vec2 uv;

        // indices
        AZStd::vector<vtx_idx> indices;
        indices.clear();
        int offset = 0;

        // tangents
        AZStd::vector<SPipTangents> tangents;
        tangents.clear();
        tangents.reserve(numVertices);

        // inspiration: https://github.com/OGRECave/ogre-procedural/blob/master/library/src/ProceduralTorusGenerator.cpp
        for (int i = 0; i <= numSegments; ++i)
        {
            // to preserve the ability to squash (using x and y radius) get distance from center based on both.
            float segmentTheta = (float)i * segmentAngleDelta;
            Vec3 v1(m_radius.GetX() * cosf(segmentTheta), m_radius.GetY() * sinf(segmentTheta), 0.f);
            float radius = v1.len();

            Vec3 tubeCenter(radius, 0.f, 0.f);
            AZ::Quaternion azq = AZ::Quaternion::CreateFromAxisAngle(AZ::Vector3(0.f, 0.f, 1.f), (float)i * segmentAngleDelta);

            quaternion q = AZQuaternionToLYQuaternion(azq);
            tubeCenter = q * tubeCenter;

            for (int j = 0; j <= numSides; ++j)
            {
                float theta = (float)j * sidesAngleDelta;
                Vec3 vertex(radius + tubeRadius*cosf(theta), 0.f, tubeRadius*sinf(theta));
                vertex = q * vertex;

                uv = Vec2((float)i / (float)(numSegments), (float)j / (float)numSides);
                vertices.push_back({ vertex, { 0xFFFFFFFF }, uv });

                // tangent will face along tube (counter clockwise ok?)
                Vec3 tangent = Vec3(0.f, 1.f, 0.f);
                tangent = q * tangent;

                // normal points in direction from tube center to the vertex on tube shell
                Vec3 normal = vertex - tubeCenter;
                normal.Normalize();

                // bitangent from cross product
                Vec3 bitangent = normal.Cross(tangent);
                bitangent.Normalize();

                // probly not necessarily but...
                tangent = normal.Cross(bitangent);
                tangent.Normalize();
                tangents.push_back(SPipTangents(tangent, bitangent, 1));

                // indices!
                if (i != numSegments)
                {
                    indices.push_back(offset + numSides + 1);
                    indices.push_back(offset);
                    indices.push_back(offset + numSides);

                    indices.push_back(offset + numSides + 1);
                    indices.push_back(offset + 1);
                    indices.push_back(offset);
                }

                ++offset;
            }
        }

        DynamicMesh::DynamicMeshRequestBus::Event(entityId, &DynamicMesh::DynamicMeshRequestBus::Events::UpdateDynamicMesh, meshId, vertices, indices, tangents);

        /*
        // This ellipse is a flat disk that always faces the camera - or it's supposed to but...well
        const uint32_t numSegments = std::max<uint32_t>(m_segments, 3);
        AZStd::vector<SVF_P3F_C4B_T2F> vertices;
        vertices.clear();
        const uint32_t numVertices = (numSegments + 1) << 1 ;
        vertices.reserve(numVertices);
       
        float startAngle = 0.f;
        float diffAngle = AZ::Constants::TwoPi;

        AZ::Transform tm;
        AZ::TransformBus::EventResult(tm, entityId, &AZ::TransformBus::Events::GetWorldTM);
        const Vec3 center = AZVec3ToLYVec3(tm.GetPosition());

        CCamera cam = gEnv->p3DEngine->GetRenderingCamera();
        //camera.GetViewMatrix();

        Matrix34_tpl<f64> mCam34 = cam.GetMatrix();
        mCam34.OrthonormalizeFast();

        Matrix44_tpl<f64> mCam44T = mCam34.GetTransposed();
        Matrix44_tpl<f64> mView64;
        mathMatrixLookAtInverse(&mView64, &mCam44T);

        Matrix44 mView = (Matrix44_tpl<f32>)mView64;

        // Rotate around x-axis by -PI/2
        Matrix44A mViewFinal = mView;
        mViewFinal.m01 = mView.m02;
        mViewFinal.m02 = -mView.m01;
        mViewFinal.m11 = mView.m12;
        mViewFinal.m12 = -mView.m11;
        mViewFinal.m21 = mView.m22;
        mViewFinal.m22 = -mView.m21;
        mViewFinal.m31 = mView.m32;
        mViewFinal.m32 = -mView.m31;

        uint32_t overlayWidth = gEnv->pRenderer->GetOverlayWidth();

        // probably need to modify this viewmatrix so it takes into account the models position
        Matrix44A viewMatrix, projMatrix;
        gEnv->pRenderer->GetModelViewMatrix(&viewMatrix.m00);
        gEnv->pRenderer->GetProjectionMatrix(&projMatrix.m00);

        viewMatrix = mViewFinal;
        Matrix44A viewMatrixInvert = viewMatrix.GetInverted();
        
        const float clipThreshold = 0.1f;

        // vertices
        for (int i = 0; i < numSegments; ++i)
        {
            SVF_P3F_C4B_T2F vertex;
            Vec2 uv;

            float theta = startAngle + diffAngle * ((float)i / (float)numSegments);
            Vec3 v1(center.x + m_radius.GetX() * cosf(theta), center.y + m_radius.GetY() * sinf(theta), center.z);

            float theta2 = startAngle + diffAngle * ((float)(i + 1) / (float)numSegments);
            Vec3 v2(center.x + m_radius.GetX() * cosf(theta2), center.y + m_radius.GetY() * sinf(theta2), center.z);

            if (i >= numSegments / 2)
            {
                //AZStd::swap<Vec3>(v1, v2);
            }
            //if (i == numSegments)
            //{
            //    theta2 = startAngle + diffAngle * ((float)(i - 1) / (float)numSegments);
            //    v2 = Vec3(center.x + m_radius.GetX() * cosf(theta2), center.y + m_radius.GetY() * sinf(theta2), center.z);
            //}

            // compute depth corrected thickness of line end points
            //float thickness = 0.5f * m_thickness * ComputeConstantScale(v1, viewMatrix, projMatrix, overlayWidth);
            float thickness = 0.2f;
            //float thicknessV1(0.5f * thickness * ComputeConstantScale(v[1], GetCurrentView(), GetCurrentProj(), m_wndXRes));


            // compute camera space line delta
            Vec4 vt[2];
            mathVec3TransformF(&vt[0], &v1, &viewMatrix);
            mathVec3TransformF(&vt[1], &v2, &viewMatrix);
            vt[0].z = (float)fsel(-vt[0].z - clipThreshold, vt[0].z, -clipThreshold);
            vt[1].z = (float)fsel(-vt[1].z - clipThreshold, vt[1].z, -clipThreshold);
            Vec4 tmp(vt[1] / vt[1].z - vt[0] / vt[0].z);
            Vec2 delta(tmp.x, tmp.y);

            // create screen space normal of line delta
            Vec2 normalVec(-delta.y, delta.x);
            mathVec2NormalizeF(&normalVec, &normalVec);
            Vec2 n(normalVec.x, normalVec.y);

            n *= thickness;

            Vec4 vc[2] =
            {
                Vec4(vt[0].x + n.x, vt[0].y + n.y, vt[0].z, vt[0].w),
                Vec4(vt[0].x - n.x, vt[0].y - n.y, vt[0].z, vt[0].w),
            };

            // compute final world space vertices of thick line
            Vec3 t1,t2;
            mathVec4TransformF(&t1, &vc[0], &(viewMatrixInvert));
            mathVec4TransformF(&t2, &vc[1], &(viewMatrixInvert));

            // TODO probably need to transpose the verts back to the origin
            {
                uv = Vec2(0.f, (float)i / (float)(numSegments));
                vertices.push_back({ Vec3(t1.x - center.x, t1.y - center.y, t1.z -center.z), {0xFFFFFFFF}, uv });

                uv = Vec2(1.f, (float)i / (float)(numSegments));
                vertices.push_back({ Vec3(t2.x - center.x, t2.y - center.y, t2.z - center.z), {0xFFFFFFFF}, uv });
            }
        }

        // copy the first two vertices back in
        vertices.push_back(vertices[0]);
        vertices.push_back(vertices[1]);

        // tangents
        AZStd::vector<SPipTangents> tangents;
        tangents.clear();
        tangents.reserve(numVertices);

        // indices
        AZStd::vector<vtx_idx> indices;
        indices.clear();

        // indices
        const uint32_t numTriangles = numSegments << 1;
        indices.reserve(numTriangles * 3);
        int i = 0;
        //for (; i < numVertices; i += 2)
        for (int i = 0, segment = 0; segment < numSegments; ++segment, i+=2)
        {
            indices.push_back(i + 0);
            indices.push_back(i + 1);
            indices.push_back(i + 2);

            indices.push_back(i + 2);
            indices.push_back(i + 1);
            indices.push_back(i + 3);
        }

        // tangents
        for (int i = 0; i < numVertices; i += 2)
        {
            //Vec3 tangent = vertices[i + 1].xyz - vertices[i + 0].xyz;
            Vec3 tangent = vertices[i].xyz - Vec3(0, 0, 0);
            tangent.Normalize();
            // for now assume straight down
            Vec3 normal = Vec3(0, 0, -1);
            Vec3 bitangent = normal.Cross(tangent);
            bitangent.Normalize();
            tangent = normal.Cross(bitangent);
            tangent.Normalize();
            tangents.push_back(SPipTangents(tangent, bitangent, 1));

            // use same tangent for both
            tangents.push_back(SPipTangents(tangent, bitangent, 1));
        }

        DynamicMesh::DynamicMeshRequestBus::Event(entityId, &DynamicMesh::DynamicMeshRequestBus::Events::UpdateDynamicMesh, meshId, vertices, indices, tangents);
        */

    }

    void Disk::CreateMesh(const AZ::EntityId entityId, DynamicMesh::DynamicMeshId meshId) const
    {
        const uint32_t numSegments = std::max<uint32_t>(m_segments, 3);
        AZStd::vector<SVF_P3F_C4B_T2F> vertices;
        vertices.clear();
        const uint32_t numVertices = m_innerRadius > 0.0 ? (numSegments + 1) << 1 : numSegments + 1;
        vertices.reserve(numVertices);


        float startAngle = 0.f;
        float diffAngle = AZ::Constants::TwoPi;
        const Vec3 center(0.f, 0.f, 0.f);

        // vertices
        for (int i = 0; i <= numSegments; ++i)
        {
            float theta = startAngle + diffAngle * ((float)i / (float)numSegments);
            SVF_P3F_C4B_T2F vertex;
            Vec2 uv;
            if (m_innerRadius > 0.0)
            {
                Vec3 innerVertex(center.x + m_innerRadius * cosf(theta), center.y + m_innerRadius * sinf(theta), 0.f);
                uv = Vec2(0.f, (float)i / (float)(numSegments));
                vertices.push_back({ innerVertex, {0xFFFFFFFF}, uv });
            }

            Vec3 outerVertex(center.x + m_outerRadius * cosf(theta), center.y + m_outerRadius * sinf(theta), 0.f);
            uv = m_innerRadius > 0.0 ? Vec2(1.f, (float)i / (float)(numSegments)) : Vec2(outerVertex.x, outerVertex.y);
            vertices.push_back({ outerVertex, {0xFFFFFFFF}, uv });
        }

        // tangents
        AZStd::vector<SPipTangents> tangents;
        tangents.clear();
        tangents.reserve(numVertices);

        // indices
        AZStd::vector<vtx_idx> indices;
        indices.clear();

        if (m_innerRadius > 0.0)
        {
            // indices
            const uint32_t numTriangles = numSegments << 1;
            indices.reserve(numTriangles * 3);
            int i = 0;
            //for (; i < numVertices; i += 2)
            for (int segment = 0; segment < numSegments; ++segment, i += 2)
            {
                indices.push_back(i + 0);
                indices.push_back(i + 1);
                indices.push_back(i + 2);

                indices.push_back(i + 2);
                indices.push_back(i + 1);
                indices.push_back(i + 3);
            }

            // tangents
            for (int i = 0; i < numVertices; i += 2)
            {
                //Vec3 tangent = vertices[i + 1].xyz - vertices[i + 0].xyz;
                Vec3 tangent = vertices[i].xyz - Vec3(0,0,0);
                tangent.Normalize();
                // for now assume straight down
                Vec3 normal = Vec3(0, 0, -1);
                Vec3 bitangent = normal.Cross(tangent);
                bitangent.Normalize();
                tangent = normal.Cross(bitangent);
                tangent.Normalize();
                tangents.push_back(SPipTangents(tangent, bitangent, 1));

                // use same tangent for both
                tangents.push_back(SPipTangents(tangent, bitangent, 1));
            }
        }
        else
        {
            const uint32_t numTriangles = numSegments - 2; 
            indices.reserve(numTriangles * 3);
            // essentially a fan but using indices
            for (int i = 0; i < numTriangles; ++i)
            {

                indices.push_back(0);
                indices.push_back(i + 1);
                indices.push_back(i + 2);
            }

            // tangents
            for (int i = 0; i < numVertices; ++i)
            {
                Vec3 tangent = vertices[i].xyz - Vec3(0,0,0);
                tangent.Normalize();
                Vec3 normal = Vec3(0, 0, -1);
                Vec3 bitangent = normal.Cross(tangent);
                bitangent.Normalize();
                tangent = normal.Cross(bitangent);
                tangent.Normalize();
                tangents.push_back(SPipTangents(tangent, bitangent, 1));
            }
        }

        DynamicMesh::DynamicMeshRequestBus::Event(entityId, &DynamicMesh::DynamicMeshRequestBus::Events::UpdateDynamicMesh, meshId, vertices, indices, tangents);
    }

    void Plane::CreateMesh(const AZ::EntityId entityId, DynamicMesh::DynamicMeshId meshId) const
    {
        AZStd::vector<SVF_P3F_C4B_T2F> vertices = {
            {{0.f,0.f,0.f}, {0xFFFFFFFF}, {0.f,0.f}},
            {{0.f,m_height,0.f}, {0xFFFFFFFF}, {1.f,0.f}},
            {{m_width,0.f,0.f}, {0xFFFFFFFF}, {0.f,1.f}},
            {{m_width,m_height,0.f}, {0xFFFFFFFF}, {1.f,1.f}}
        };

        AZStd::vector<vtx_idx> indices = 
        {
            0, 2, 1, // counter clock wise (CCW)
            2, 3, 1
        };

        AZStd::vector<SPipTangents> tangents;
        tangents.clear();
        tangents.reserve(4);

        Vec3 tangent = vertices[2].xyz - vertices[0].xyz;
        tangent.Normalize();
        Vec3 bitangent = vertices[1].xyz - vertices[0].xyz;
        bitangent.Normalize();
        Vec3 normal = Vec3(0, 0, -1);
        bitangent = normal.Cross(tangent);
        bitangent.Normalize();
        tangent = normal.Cross(bitangent);
        tangent.Normalize();
        tangents.push_back(SPipTangents(tangent, bitangent,1));
        tangents.push_back(tangents[0]);
        tangents.push_back(tangents[0]);
        tangents.push_back(tangents[0]);

        DynamicMesh::DynamicMeshRequestBus::Event(entityId, &DynamicMesh::DynamicMeshRequestBus::Events::UpdateDynamicMesh, meshId, vertices, indices, tangents);

        /*
        // create the mesh
        const SVF_P3F_C4B_T2F vertices[] =
        {
            {{0.f,0.f,0.f}, {0xFFFFFFFF}, {0.f,0.f}},
            {{0.f,1.f,0.f}, {0xFFFFFFFF}, {1.f,0.f}},
            {{1.f,0.f,0.f}, {0xFFFFFFFF}, {0.f,1.f}},
            {{1.f,1.f,0.f}, {0xFFFFFFFF}, {1.f,1.f}}
        };

        const vtx_idx indices[] = 
        {
            0, 2, 1, // CCW
            2, 3, 1
        };
        
        // tangents! unless you like meshes with no lighting
        std::vector<SPipTangents> tangents;
        tangents.clear();
        tangents.reserve(4);

        Vec3 tangent = vertices[2].xyz - vertices[0].xyz;
        Vec3 bitangent = vertices[1].xyz - vertices[0].xyz;
        Vec3 normal = Vec3(0, 0, -1);
        bitangent = normal.Cross(tangent);
        bitangent.Normalize();
        tangent = normal.Cross(bitangent);
        tangent.Normalize();
        tangents.push_back(SPipTangents(tangent, bitangent,1));
        tangents.push_back(tangents[0]);
        tangents.push_back(tangents[0]);
        tangents.push_back(tangents[0]);

        const uint32 vertexCount = sizeof(vertices) / sizeof(vertices[0]);
        const uint32 indexCount = sizeof(indices) / sizeof(indices[0]);


        SVF_P3F_C4B_T2F vert;
        Vec3 vNDC;

        std::vector<uint32> indBuff;
        indBuff.clear();
        indBuff.reserve(36);

        std::vector<SVF_P3F_C4B_T2F> vertBuff;
        vertBuff.clear();
        vertBuff.reserve(8);

        //Create unit box
        for (int i = 0; i < 8; i++)
        {
            //Generate screen space frustum (CCW faces)
            vNDC = Vec3((i == 0 || i == 1 || i == 4 || i == 5) ? 0.0f : 1.0f,
                (i == 0 || i == 3 || i == 4 || i == 7) ? 0.0f : 1.0f,
                (i == 0 || i == 1 || i == 2 || i == 3) ? 0.0f : 1.0f
            );
            vert.xyz = vNDC;
            vert.st = Vec2(0.0f, 0.0f);
            vert.color.dcolor = -1;
            vertBuff.push_back(vert);
        }

        //CCW faces
        uint16 nFaces[6][4] = {
            { 0, 1, 2, 3 },
            { 4, 7, 6, 5 },
            { 0, 3, 7, 4 },
            { 1, 5, 6, 2 },
            { 0, 4, 5, 1 },
            { 3, 2, 6, 7 }
        };

        //init indices for triangles drawing
        for (int i = 0; i < 6; i++)
        {
            indBuff.push_back((uint16)nFaces[i][0]);
            indBuff.push_back((uint16)nFaces[i][1]);
            indBuff.push_back((uint16)nFaces[i][2]);

            indBuff.push_back((uint16)nFaces[i][0]);
            indBuff.push_back((uint16)nFaces[i][2]);
            indBuff.push_back((uint16)nFaces[i][3]);
        }

        const AZ::Vertex::Format vertexFormat = eVF_P3F_C4B_T2F;
        const PublicRenderPrimitiveType primitiveType = prtTriangleList;
        //const PublicRenderPrimitiveType primitiveType = prtTriangleStrip;
        ERenderMeshType bufferType = eRMT_Static;

        //m_mesh = gEnv->pRenderer->CreateRenderMeshInitialized(
        //    &vertBuff[0], vertBuff.size(), vertexFormat, 
        //    &indBuff[0], indBuff.size(),
        //    primitiveType, "TestMeshType", "TestMesh");

        // Create a new rendermesh and dispatch the asynchronous updates
        m_mesh = gEnv->pRenderer->CreateRenderMeshInitialized(
            vertices, vertexCount, vertexFormat, 
            indices, indexCount,
            primitiveType, "TestMeshType", "TestMesh",bufferType,1,0,nullptr,nullptr,false,true,&tangents[0]);

        //m_mesh = gEnv->pRenderer->CreateRenderMeshInitialized(
        //    vertices, vertexCount, vertexFormat, 
        //    indices, indexCount,
        //    primitiveType, "TestMeshType", "TestMesh");

        if (m_mesh)
        {
            if (m_material)
            {
                //m_mesh->SetChunk(m_material, 0, vertBuff.size(), 0, indBuff.size(), 1.f, vertexFormat);
                m_mesh->SetChunk(m_material, 0, vertexCount, 0, indexCount, 1.f, vertexFormat);
            }
            //m_mesh->SetBBox(Vec3(-1, -1, -1), Vec3(1, 1, 1));
        }


        // need this?
        //m_mesh->SetBBox();

        // could use eRMT_Dynamic and write data dynamically too like MergedMeshRenderNode::CreateRenderMesh
        //if (m_mesh)
        //{
        //    m_mesh->LockForThreadAccess();
        //}
        */
    }

    void ProceduralMeshComponent::Reflect(AZ::ReflectContext* context)
    {
        //ProceduralMeshComponentConfig::Reflect(context);

        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<Primitive>()
                ->Version(1)
                ;

            serialize->Class<Plane, Primitive>()
                ->Version(1)
                ->Field("Plane Width", &Plane::m_width)
                ->Field("Plane Height", &Plane::m_height)
                ->Field("Plane Segments", &Plane::m_segments)
                ;

            serialize->Class<Disk, Primitive>()
                ->Version(1)
                ->Field("Disk Inner Radius", &Disk::m_innerRadius)
                ->Field("Disk Outer Radius", &Disk::m_outerRadius)
                ->Field("Disk Segments", &Disk::m_segments)
                ;

            serialize->Class<Torus, Primitive>()
                ->Version(1)
                ->Field("Radius", &Torus::m_radius)
                ->Field("Thickness", &Torus::m_thickness)
                ->Field("Segments", &Torus::m_segments)
                ->Field("Sides", &Torus::m_sides)
                ;

            serialize->Class<ProceduralMeshComponent, AZ::Component>()
                ->Version(1)
                ->Field("Primitive", &ProceduralMeshComponent::m_primitive)
                ;
        }

        if (AZ::BehaviorContext* behaviorContext = azrtti_cast<AZ::BehaviorContext*>(context))
        {
            using Primitive = ProceduralMesh::Primitive;
            behaviorContext->Class<Primitive>("ProceduralPrimitive")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ;

            behaviorContext->Class<Plane>("ProceduralPlane")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ->Property("width", BehaviorValueProperty(&Plane::m_width))
                ->Property("height", BehaviorValueProperty(&Plane::m_height))
                ->Property("segments", BehaviorValueProperty(&Plane::m_segments))
                ;

            behaviorContext->Class<Disk>("ProceduralDisk")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ->Property("innerRadius", BehaviorValueProperty(&Disk::m_innerRadius))
                ->Property("outerRadius", BehaviorValueProperty(&Disk::m_outerRadius))
                ->Property("segments", BehaviorValueProperty(&Disk::m_segments))
                ;

            behaviorContext->Class<Torus>("ProceduralTorus")
                ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                ->Property("radius", BehaviorValueProperty(&Torus::m_radius))
                ->Property("thickness", BehaviorValueProperty(&Torus::m_thickness))
                ->Property("segments", BehaviorValueProperty(&Torus::m_segments))
                ->Property("sides", BehaviorValueProperty(&Torus::m_sides))
                ;

            behaviorContext->Class<ProceduralMeshComponent>()
                ->RequestBus("ProceduralPrimitiveRequestBus");

            behaviorContext->EBus<ProceduralPrimitiveRequestBus>("ProceduralPrimitiveRequestBus")
                ->Event("SetPrimitive", &ProceduralPrimitiveRequestBus::Events::SetPrimitive)
                ;
        }
    }

    void ProceduralMeshComponent::SetPrimitive(const Primitive& primitive)
    {
        m_primitive = primitive.GetShared();

        if (m_primitive && m_primitive->GetType() != PrimitiveType::Empty)
        {
            if (m_meshId.IsNull())
            {
                DynamicMesh::DynamicMeshRequestBus::EventResult(m_meshId, GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::CreateDynamicMesh);
                if (m_meshId.IsNull())
                {
                    AZ_Warning("ProceduralMeshComponent", false, "Failed to create a dynamic mesh.");
                    return;
                }
            }

            m_primitive->CreateMesh(GetEntityId(), m_meshId);
        }
        else if (!m_meshId.IsNull())
        {
            DynamicMesh::DynamicMeshRequestBus::Event(GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::DestroyDynamicMesh, m_meshId);
            m_meshId = DynamicMesh::DynamicMeshId::CreateNull();
        }
    }

    void ProceduralMeshComponent::Init()
    {
    }

    void ProceduralMeshComponent::Activate()
    {
        ProceduralPrimitiveRequestBus::Handler::BusConnect(GetEntityId());

        if (m_primitive && m_primitive->GetType() != PrimitiveType::Empty)
        {
            DynamicMesh::DynamicMeshRequestBus::EventResult(m_meshId, GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::CreateDynamicMesh);
            if (m_meshId.IsNull())
            {
                AZ_Warning("ProceduralMeshComponent", false, "Failed to create a dynamic mesh.");
                return;
            }

            m_primitive->CreateMesh(GetEntityId(), m_meshId);
        }
    }

    void ProceduralMeshComponent::Deactivate()
    {
        if (!m_meshId.IsNull())
        {
            DynamicMesh::DynamicMeshRequestBus::Event(GetEntityId(), &DynamicMesh::DynamicMeshRequestBus::Events::DestroyDynamicMesh, m_meshId);
            m_meshId = DynamicMesh::DynamicMeshId::CreateNull();
        }

        ProceduralPrimitiveRequestBus::Handler::BusDisconnect();
    }

    /*
    bool ProceduralMeshComponent::ReadInConfig(const AZ::ComponentConfig* baseConfig)
    {
        if (const ProceduralMeshComponentConfig* config = azrtti_cast<const ProceduralMeshComponentConfig*>(baseConfig))
        {
            //static_cast<ProceduralMeshComponentConfig>(m_config) = *config;
            if (config->m_primitiveType == PrimitiveType::Plane)
            {
                m_primitive = AZStd::make_unique<Primitive>(config->m_plane);
            }
            else if (config->m_primitiveType == PrimitiveType::Disk)
            {
                m_primitive = AZStd::make_unique<Primitive>(config->m_disk);
            }
            else if (m_primitive)
            {
                m_primitive.release();
            }
            return true;
        }
        return false;
    }

    bool ProceduralMeshComponent::WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const
    {
        //if (auto config = azrtti_cast<ProceduralMeshComponentConfig*>(outBaseConfig))
        //{
        //    *config = m_config;
        //    return true;
        //}
        return false;
    }
    */


}
