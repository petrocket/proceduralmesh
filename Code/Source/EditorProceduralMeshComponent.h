
#pragma once

#include <AzToolsFramework/ToolsComponents/EditorComponentBase.h>
#include <AzToolsFramework/ToolsComponents/EditorVisibilityBus.h>
#include <AzFramework/Entity/EntityDebugDisplayBus.h>

#include "ProceduralMeshComponent.h"

namespace ProceduralMesh 
{
    /*
    class EditorProceduralMeshComponentConfig
        : public ProceduralMeshComponentConfig
    {
    public:
        AZ_CLASS_ALLOCATOR(EditorProceduralMeshComponentConfig, AZ::SystemAllocator, 0);
        AZ_RTTI(EditorProceduralMeshComponentConfig, "{C9BDAE0B-9064-48B1-93CF-3F4FFC0A2C51}", ProceduralMeshComponentConfig);

        static void Reflect(AZ::ReflectContext* context);

        AZ::Crc32 GetDiskVisibility() const;
        AZ::Crc32 GetPlaneVisibility() const;
        AZ::Crc32 GetVisibility(PrimitiveType type) const;
    };
    */

    class EditorProceduralMeshComponent
        : public AzToolsFramework::Components::EditorComponentBase
        , protected ProceduralMesh::ProceduralPrimitiveRequestBus::Handler
    {
    public:
        AZ_EDITOR_COMPONENT(EditorProceduralMeshComponent, "{77620264-F147-4151-96B6-1606543C2A33}");

        ~EditorProceduralMeshComponent() override = default;

        static void Reflect(AZ::ReflectContext* context);

        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
        {
            required.push_back(AZ_CRC("DynamicMeshService", 0xb2381405));
        }

        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
        {
            dependent.push_back(AZ_CRC("DynamicMeshService", 0xb2381405));
        }

    protected:
        //////////////////////////////////////////////////////////////////////////
        // EditorComponentBase
        void BuildGameEntity(AZ::Entity* gameEntity) override;
        //bool ReadInConfig(const AZ::ComponentConfig* baseConfig) override;
        //bool WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const override;
        //////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // ProceduralMesh::ProceduralPrimitiveRequestBus::Handler 
        void SetPrimitive(const Primitive& primitive) override;
        ////////////////////////////////////////////////////////////////////////

        AZ::Crc32 OnPrimitiveTypeChanged();
        AZ::Crc32 OnPrimitiveChanged();

    private:
        //EditorProceduralMeshComponentConfig m_config;

        // the primitive type to use
        PrimitiveType m_primitiveType = PrimitiveType::Empty;

        // disk primitive
        Disk m_disk;

        // plane primitive
        Plane m_plane;

        // ellipse primitive
        Torus m_torus;

        AZ::Crc32 GetDiskVisibility() const;
        AZ::Crc32 GetTorusVisibility() const;
        AZ::Crc32 GetPlaneVisibility() const;
        AZ::Crc32 GetVisibility(PrimitiveType type) const;

        DynamicMesh::DynamicMeshId m_meshId;
    };
}