
#pragma once

#include <AzCore/Component/Component.h>
//#include <AzCore/Math/Crc.h>

#include "DynamicMesh/DynamicMeshBus.h"
#include "ProceduralMesh/ProceduralMeshBus.h"

namespace ProceduralMesh 
{
    //class ProceduralMeshComponentConfig
    //    : public AZ::ComponentConfig
    //{
    //public:
    //    AZ_CLASS_ALLOCATOR(ProceduralMeshComponentConfig, AZ::SystemAllocator, 0);
    //    AZ_RTTI(ProceduralMeshComponentConfig, "{157B2608-0887-453B-A310-EDCFBDDE0B5D}", ComponentConfig);

    //    static void Reflect(AZ::ReflectContext* context);

    //    // the primitive type to use
    //    PrimitiveType m_primitiveType = PrimitiveType::Empty;

    //    // disk primitive
    //    Disk m_disk;

    //    // plane primitive
    //    Plane m_plane;
    //};

    class ProceduralMeshComponent
        : public AZ::Component
        , protected ProceduralMesh::ProceduralPrimitiveRequestBus::Handler
    {
    public:
        AZ_COMPONENT(ProceduralMeshComponent, "{7E8C6827-8785-4D71-B634-16D793BAAC43}");

        ProceduralMeshComponent() = default;
        ProceduralMeshComponent(const Primitive& primitive);
        ~ProceduralMeshComponent() override = default;

        static void Reflect(AZ::ReflectContext* context);

        static void ProceduralMeshComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
        {
            required.push_back(AZ_CRC("DynamicMeshService"));
        }

        static void ProceduralMeshComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
        {
            dependent.push_back(AZ_CRC("DynamicMeshService"));
        }

    protected:
        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        //bool ReadInConfig(const AZ::ComponentConfig* baseConfig) override;
        //bool WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // ProceduralMesh::ProceduralPrimitiveRequestBus::Handler 
        void SetPrimitive(const Primitive& primitive) override;
        ////////////////////////////////////////////////////////////////////////

    private:
        AZStd::shared_ptr<Primitive> m_primitive;

        // the identifier of the dynamic mesh to use
        DynamicMesh::DynamicMeshId m_meshId;
    };
}